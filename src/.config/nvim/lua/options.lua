-------------------------------------------------------------------------------
-- Description: Neovim general configurations
-- Author: iDes3rt <ides3rt@protonmail.com>
-- Source: https://codeberg.org/ides3rt/Dotfiles
-- Last Modified: 2022-12-26
-------------------------------------------------------------------------------

local set = vim.opt

if os.getenv('DISPLAY') ~= nil then
	set.clipboard = { 'unnamedplus', 'unnamed' }
end

set.complete:append { 'kspell' }
set.completeopt = { 'menuone', 'noinsert', 'noselect' }
set.cursorlineopt = { 'number' }
set.dictionary = { '/usr/share/dict/words' }
set.diffopt:append { 'vertical' }
set.fillchars = { vert = ' ', fold = ' ' }
set.matchpairs:append { '<:>' }
set.shortmess:append('cS')
set.whichwrap = 'b,s,h,l,<,>,~,[,]'

set.autochdir = true
set.backupcopy = 'yes'
set.backupskip = { '/tmp/*', '/var/tmp/*' }
set.fsync = true
set.icon = true
set.matchtime = 0
set.modeline = false
set.modelines = 0
set.mouse = ''
set.numberwidth = 2
set.path = { '**' }
set.report = 0
set.scrolloff = 999
set.shada = { '' }
set.shadafile = { 'NONE' }
set.showcmd = false
set.sidescroll = 0
set.sidescrolloff = 999
set.splitbelow = true
set.splitright = true
set.swapfile = false
set.textwidth = 79
set.timeout = false
set.title = true
set.ttimeout = true
set.ttimeoutlen = 0
set.undofile = true
set.wrap = false
