-------------------------------------------------------------------------------
-- Description: Neovim keymaps-related configurations
-- Author: iDes3rt <ides3rt@protonmail.com>
-- Source: https://codeberg.org/ides3rt/Dotfiles
-- Last Modified: 2022-09-24
-------------------------------------------------------------------------------

vim.g.mapleader = ' '

local function map(mode, lhs, rhs, args)
	local arguments = { noremap = true }

	if mode ~= 'c' and mode ~= 'l' then
		arguments = vim.tbl_extend(
			'force', arguments, { silent = true }
		)
	end

	if args then
		arguments = vim.tbl_extend('force', arguments, args)
	end

	vim.api.nvim_set_keymap(mode, lhs, rhs, arguments)
end

map('', 'H', '^')
map('', 'L', 'g_')

map('', 'j', 'gj')
map('', 'k', 'gk')

map('n', '<C-J>', ':move +1<CR>')
map('n', '<C-K>', ':move -2<CR>')

map('n', '<Leader>sw', ':setlocal wrap! linebreak!<CR>')
map('n', '<Leader>sp', ':setlocal spell!<CR>')
map('n', '<Leader>ic', ':set ignorecase!<CR>')

map('n', '<Leader>nn', ':next<Bar>echo @%<CR>')
map('n', '<Leader>pp', ':prev<Bar>echo @%<CR>')

map('n', '<Leader><Space>', ':let @/ = ""<CR>')

map('n', '<C-R>', '<Nop>')
map('n', 'U', ':redo<CR>')

map('n', 'Q', 'gq')
map('n', 'Y', 'y$')
map('n', 'gQ', '<Nop>')

map('v', '<C-J>', ":move '>+1<CR>gv")
map('v', '<C-K>', ":move '<-2<CR>gv")

map('v', '<', '<gv')
map('v', '>', '>gv')

map('i', '<C-J>', '<C-G>u<Cmd>move +1<CR>')
map('i', '<C-K>', '<C-G>u<Cmd>move -2<CR>')

map('i', '<C-U>', '<C-G>u<C-U>')

map('i', '!', '!<C-G>u')
map('i', ',', ',<C-G>u')
map('i', '.', '.<C-G>u')
map('i', '?', '?<C-G>u')

map('c', '<C-A>', '<Home>')
map('c', '<C-B>', '<Left>')
map('c', '<C-E>', '<End>')
map('c', '<C-F>', '<Right>')
