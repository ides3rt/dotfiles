-------------------------------------------------------------------------------
-- Description: Configuration for Nvim-Treesitter
-- Author: iDes3rt <ides3rt@protonmail.com>
-- Source: https://codeberg.org/ides3rt/Dotfiles
-- Last Modified: 2022-08-11
-------------------------------------------------------------------------------

require('nvim-treesitter.configs').setup {
	ensure_installed = 'all',
	sync_install = false,

	highlight = {
		enable = true,
		disable = { 'bash', 'vim' },
		additional_vim_regex_highlighting = false
	}
}

vim.opt.foldenable = false
vim.opt.foldmethod = 'expr'
vim.opt.foldexpr = 'nvim_treesitter#foldexpr()'
