-------------------------------------------------------------------------------
-- Description: Disable `xscreensaver` while `mpv` is running
-- Author: iDes3rt <ides3rt@protonmail.com>
-- Source: https://codeberg.org/ides3rt/Dotfiles
-- Last Modified: 2022-12-28
-------------------------------------------------------------------------------

local utils = require('mp.utils')
mp.add_periodic_timer(30, function()
	utils.subprocess({args={"xscreensaver-command", "-deactivate"}})
end)
