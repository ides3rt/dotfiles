#!/usr/bin/env -S bash -p
#------------------------------------------------------------------------------
# Description: Key bindings for Bash (Vi-Mode)
# Author: iDes3rt <ides3rt@protonmail.com>
# Source: https://codeberg.org/ides3rt/Dotfiles
# Last Modified: 2022-12-26
#------------------------------------------------------------------------------

{
	[[ $- != *i* ]] ||
		shopt -q restricted_shell
} && return 0

stty quit '' eof '' start '' stop '' rprnt '' \
	lnext '' discard '' -ixon -ixoff

bind '"\eOB": history-search-forward'
bind '"\eOA": history-search-backward'

bind '"\e[B": history-search-forward'
bind '"\e[A": history-search-backward'

bind '"\e[1;2C": forward-word'
bind '"\e[1;2D": backward-word'

bind 'TAB: menu-complete'
bind '"\e[Z": menu-complete-backward'

bind '\C-l: clear-display'

if (( VI_MODE )); then
	set -o vi

	bind -m vi -r "\C-r"
	bind -r "\C-d"
	bind -r "\C-v"
	bind -r "\C-y"

	bind -u backward-word
	bind -u forward-word
	bind -u kill-word
	bind -u menu-complete
	bind -u menu-complete-backward
	bind -u overwrite-mode
	bind -u transpose-chars

	bind -m vi 'U: vi-redo'

	bind -m vi 'j: history-search-forward'
	bind -m vi 'k: history-search-backward'
fi
