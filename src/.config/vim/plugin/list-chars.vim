"------------------------------------------------------------------------------
" Description: Toggle the enable of Tab in 'listchars' for Vim
" Author: iDes3rt <ides3rt@protonmail.com>
" Source: https://codeberg.org/ides3rt/Dotfiles
" Last Modified: 2022-06-18
"------------------------------------------------------------------------------

if exists('g:loaded_listchars_plugin')
	finish
endif

set list lcs=extends:#,tab:\ \ ,nbsp:.

func! ToggleListChars()
	if !exists('g:listchars_tab')
		let g:listchars_tab = 1
		set listchars=extends:#,tab:│\ ,nbsp:.

	elseif g:listchars_tab == 1
		unlet g:listchars_tab
		set listchars=extends:#,tab:\ \ ,nbsp:.

	endif
endfunc

nnoremap <silent> <Leader>ls :call ToggleListChars()<CR>
let g:loaded_listchars_plugin = 1
