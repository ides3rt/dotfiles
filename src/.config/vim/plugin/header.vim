"------------------------------------------------------------------------------
" Description: Place a snippet version of headers
" Author: iDes3rt <ides3rt@protonmail.com>
" Source: https://codeberg.org/ides3rt/Dotfiles
" Last Modified: 2022-06-25
"------------------------------------------------------------------------------

if exists('g:loaded_header_plugin')
	finish
endif

func! PlaceHeader()
	mark c

	exec "norm! i#\<Esc>78a-\<Esc>o"
	exec "norm! i# Description:\<CR>"
	exec "norm! i# Author: iDes3rt <ides3rt@protonmail.com>\<CR>"
	exec "norm! i# Source: https://codeberg.org/ides3rt\<CR>"
	exec "norm! i# Last Modified:\<CR>"
	exec "norm! i#\<Esc>78a-\<Esc>o"

	exec "norm! 'c"
	delm c
endfunc

nnoremap <silent> <Leader>head :call PlaceHeader()<CR>
let g:loaded_header_plugin = 1
