"------------------------------------------------------------------------------
" Description: A better way to handle auto-comment in Vim
" Author: iDes3rt <ides3rt@protonmail.com>
" Source: https://codeberg.org/ides3rt/Dotfiles
" Last Modified: 2022-04-28
"------------------------------------------------------------------------------

if exists('g:loaded_autocomment_plugin')
	finish
endif

func! BetterAutoComment()
	if exists('b:loaded_autocomment_buffer')
		return
	endif

	if &comments =~ '//'
		setlocal com=s1:/*,mb:*,ex:*/,fO://
	else
		setlocal fo-=r fo-=o
	endif

	let b:loaded_autocomment_buffer = 1
endfunc

autocmd BufWinEnter * :call BetterAutoComment()
let g:loaded_autocomment_plugin = 1
