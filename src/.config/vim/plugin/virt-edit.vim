"------------------------------------------------------------------------------
" Description: Toggle Virtual Edit in Vim
" Author: iDes3rt <ides3rt@protonmail.com>
" Source: https://codeberg.org/ides3rt/Dotfiles
" Last Modified: 2022-06-18
"------------------------------------------------------------------------------

if exists('g:loaded_virtedit_plugin')
	finish
endif

set virtualedit=block

func! VirtualEdit()
	if &virtualedit == 'block'
	       set virtualedit=all

	elseif &virtualedit == 'all'
	       set virtualedit=block
	endif
endfunc

nnoremap <silent> <leader>vr :call VirtualEdit()<CR>
let g:loaded_virtedit_plugin = 1
