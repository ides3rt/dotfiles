"------------------------------------------------------------------------------
" Description: Almost no-color color scheme  (Dark, 256 colors-palate)
" Author: iDes3rt <ides3rt@protonmail.com>
" Source: https://codeberg.org/ides3rt/Dotfiles
" Last Modified: 2022-10-04
"------------------------------------------------------------------------------

set background=dark
if exists('g:colors_name')
	highlight clear

	if exists('syntax_on')
		syntax reset
	endif
endif

let g:colors_name = 'zen-dark'

hi ColorColumn ctermfg=NONE ctermbg=233 guibg=NONE
hi Comment cterm=italic ctermfg=124 guifg=NONE
hi CursorColumn ctermbg=254 guibg=NONE
hi CursorLine cterm=NONE ctermbg=254 gui=NONE guibg=NONE
hi CursorLineNr cterm=NONE ctermfg=245 gui=NONE guifg=NONE
hi DiffAdd ctermfg=2 ctermbg=NONE guibg=NONE
hi DiffDelete ctermfg=1 ctermbg=NONE gui=NONE guifg=NONE guibg=NONE
hi FoldColumn ctermfg=243 ctermbg=233 guifg=NONE guibg=NONE
hi Folded ctermfg=243 ctermbg=233 guifg=NONE guibg=NONE
hi IncSearch cterm=reverse gui=NONE
hi Keyword cterm=bold
hi LineNr ctermfg=238 guifg=NONE
hi NonText ctermfg=124 gui=NONE guifg=NONE
hi Normal ctermfg=254 ctermbg=16
hi PmenuSel ctermfg=117 ctermbg=NONE guibg=NONE
hi Search ctermfg=186 ctermbg=NONE guifg=NONE guibg=NONE
hi SignColumn ctermfg=NONE ctermbg=233 guifg=NONE guibg=NONE
hi SpellBad cterm=underline ctermbg=NONE gui=NONE guisp=NONE
hi SpellCap cterm=underline ctermbg=NONE gui=NONE guisp=NONE
hi StatusLine cterm=NONE ctermfg=243 ctermbg=233 gui=NONE
hi StatusLineNC cterm=NONE ctermfg=243 ctermbg=233 gui=NONE
hi String ctermfg=108
hi TabLine cterm=NONE ctermfg=243 ctermbg=233 gui=NONE guibg=NONE
hi TabLineFill cterm=NONE ctermfg=243 ctermbg=233 gui=NONE
hi TabLineSel cterm=NONE ctermfg=232 ctermbg=252 gui=NONE
hi TermCursor cterm=reverse gui=NONE
hi Title ctermfg=243 ctermbg=233 gui=NONE guifg=NONE
hi Todo cterm=reverse ctermfg=NONE ctermbg=NONE guifg=NONE guibg=NONE
hi Underlined cterm=underline ctermfg=NONE gui=NONE guifg=NONE
hi VertSplit cterm=NONE ctermfg=243 ctermbg=233 gui=NONE
hi Visual ctermfg=186 ctermbg=NONE guibg=NONE
hi WildMenu ctermfg=243 ctermbg=233 guifg=NONE guibg=NONE

hi Trail ctermbg=167
match Trail '\s\+$'

hi link shQuote String

hi clear Conceal
hi clear Constant
hi clear DiagnosticError
hi clear DiagnosticHint
hi clear DiagnosticInfo
hi clear DiagnosticUnderlineError
hi clear DiagnosticUnderlineHint
hi clear DiagnosticUnderlineInfo
hi clear DiagnosticUnderlineWarn
hi clear DiagnosticWarn
hi clear DiffChange
hi clear DiffText
hi clear Directory
hi clear Error
hi clear ErrorMsg
hi clear FloatShadow
hi clear FloatShadowThrough
hi clear Identifier
hi clear Ignore
hi clear MatchParen
hi clear ModeMsg
hi clear MoreMsg
hi clear NvimInternalError
hi clear Pmenu
hi clear PmenuSbar
hi clear PmenuThumb
hi clear PreProc
hi clear Question
hi clear RedrawDebugClear
hi clear RedrawDebugComposed
hi clear RedrawDebugNormal
hi clear RedrawDebugRecompose
hi clear Special
hi clear SpecialKey
hi clear SpellLocal
hi clear SpellRare
hi clear Statement
hi clear Type
hi clear WarningMsg
