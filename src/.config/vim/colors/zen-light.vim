"------------------------------------------------------------------------------
" Description: Almost no-color color scheme  (Light, GUI-only)
" Author: iDes3rt <ides3rt@protonmail.com>
" Source: https://codeberg.org/ides3rt/Dotfiles
" Last Modified: 2022-10-04
"------------------------------------------------------------------------------

set background=light
if exists('g:colors_name')
	highlight clear

	if exists('syntax_on')
		syntax reset
	endif
endif

let g:colors_name = 'zen-light'

let g:terminal_ansi_colors = [
	\ '#fdf6e3', '#dc322f', '#859900', '#b58900', '#268bd2', '#d33682',
	\ '#2aa198', '#657b83', '#93a1a1', '#dc322f', '#859900', '#b58900',
	\ '#268bd2', '#d33682', '#2aa198', '#002b36'
\]

if has('nvim')
	let g:terminal_color_0 = '#fdf6e3'
	let g:terminal_color_1 = '#dc322f'
	let g:terminal_color_2 = '#859900'
	let g:terminal_color_3 = '#b58900'
	let g:terminal_color_4 = '#268bd2'
	let g:terminal_color_5 = '#d33682'
	let g:terminal_color_6 = '#2aa198'
	let g:terminal_color_7 = '#657b83'
	let g:terminal_color_8 = '#93a1a1'
	let g:terminal_color_9 = '#dc322f'
	let g:terminal_color_10 = '#859900'
	let g:terminal_color_11 = '#b58900'
	let g:terminal_color_12 = '#268bd2'
	let g:terminal_color_13 = '#d33682'
	let g:terminal_color_14 = '#2aa198'
	let g:terminal_color_15 = '#002b36'
endif

hi ColorColumn ctermbg=NONE guibg=#eee8d5
hi Comment ctermfg=NONE gui=italic guifg=#dc322f
hi Conceal ctermfg=NONE ctermbg=NONE guifg=#268bd2 guibg=NONE
hi CursorColumn ctermbg=NONE guibg=#eee8d5
hi CursorLine cterm=NONE ctermbg=NONE gui=NONE guibg=#eee8d5
hi CursorLineNr cterm=NONE ctermfg=NONE gui=NONE guifg=#586e75
hi DiffAdd ctermbg=NONE guifg=#859900 guibg=NONE
hi DiffDelete ctermfg=NONE ctermbg=NONE gui=NONE guifg=#dc322f guibg=NONE
hi FoldColumn ctermfg=NONE ctermbg=NONE guifg=#657b83 guibg=NONE
hi Folded ctermfg=NONE ctermbg=NONE guifg=#657b83 guibg=NONE
hi IncSearch cterm=NONE gui=reverse
hi Keyword gui=bold
hi LineNr ctermfg=NONE guifg=#93a1a1
hi NonText ctermfg=NONE gui=NONE guifg=#dc322f
hi Normal guifg=#586e75 guibg=#fdf6e3
hi PmenuSel ctermfg=NONE ctermbg=NONE guifg=#2aa198 guibg=NONE
hi Search ctermfg=NONE ctermbg=NONE guifg=#b58900 guibg=NONE
hi SignColumn ctermfg=NONE ctermbg=NONE guifg=#657b83 guibg=NONE
hi SpellBad cterm=NONE ctermbg=NONE gui=undercurl guisp=#dc322f
hi SpellCap cterm=NONE ctermbg=NONE gui=undercurl guisp=#dc322f
hi StatusLine cterm=NONE gui=NONE guifg=#073642 guibg=#eee8d5
hi StatusLineNC cterm=NONE gui=NONE guifg=#073642 guibg=#eee8d5
hi String guifg=#859900
hi TabLine cterm=NONE ctermfg=NONE ctermbg=NONE gui=NONE guifg=#93a1a1 guibg=#eee8d5
hi TabLineFill cterm=NONE gui=NONE guifg=#93a1a1 guibg=#eee8d5
hi TabLineSel cterm=NONE gui=NONE guifg=#073642 guibg=#eee8d5
hi TermCursor cterm=NONE gui=reverse
hi Title ctermfg=NONE gui=NONE guifg=#cb4b16
hi Todo ctermfg=NONE ctermbg=NONE gui=reverse guifg=NONE guibg=NONE
hi Underlined cterm=NONE ctermfg=NONE gui=underline guifg=NONE
hi VertSplit cterm=NONE gui=NONE guifg=#93a1a1 guibg=#eee8d5
hi Visual ctermbg=NONE guifg=#b58900 guibg=NONE
hi WildMenu ctermfg=NONE ctermbg=NONE guifg=#fdf6e3 guibg=#93a1a1

hi Trail guibg=#dc322f
match Trail '\s\+$'

hi link shQuote String

hi clear Constant
hi clear DiagnosticError
hi clear DiagnosticHint
hi clear DiagnosticInfo
hi clear DiagnosticUnderlineError
hi clear DiagnosticUnderlineHint
hi clear DiagnosticUnderlineInfo
hi clear DiagnosticUnderlineWarn
hi clear DiagnosticWarn
hi clear DiffChange
hi clear DiffText
hi clear Directory
hi clear Error
hi clear ErrorMsg
hi clear FloatShadow
hi clear FloatShadowThrough
hi clear Identifier
hi clear Ignore
hi clear MatchParen
hi clear ModeMsg
hi clear MoreMsg
hi clear NvimInternalError
hi clear Pmenu
hi clear PmenuSbar
hi clear PmenuThumb
hi clear PreProc
hi clear Question
hi clear RedrawDebugClear
hi clear RedrawDebugComposed
hi clear RedrawDebugNormal
hi clear RedrawDebugRecompose
hi clear Special
hi clear SpecialKey
hi clear SpellLocal
hi clear SpellRare
hi clear Statement
hi clear Type
hi clear WarningMsg
